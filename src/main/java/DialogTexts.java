public enum DialogTexts {

    WelcomeText("Witaj! Przed Tobą aplikacja, dzięki której nauczysz się sprawniej pisać na klawiaturze. Aby rozpocząć wciśnij dowolny przycisk klawiatury."),
    AskUserAboutFilePathText("Wprowadź ścieżkę do pliku tekstowego, który będziesz przepisywać. " +
            "Puste linie zostaną pominięte." +
            "\nAby zapobiec komplikacjom wyświetlania tekstu i sprawdzania poprawności wprowadzonego tekstu, plik powinien być zakodowany w formacie UTF-8(bez BOM). " +
            "\nPrzykładowy szablon poprawnej ścieżki to: C:\\\\Użytkownicy\\Albert\\Dokumenty\\plik.txt"),
    AskUserAboutNameText("Aby rozpocząć, wprowadź swoje imię:  "),
    LineLengthErrorText("Wprowdzono niepoprawną ilość znaków w linii. "),
    MistakeInUserLineText("Pojawił się conajmniej jeden błąd. Pierwszy z nich znajduje się w znaku nr "),
    CongratulationText("Gratulacje!"),
    TypingTimeInfoText("Zajęło Ci to (sekund): ");

    private String message;

    DialogTexts(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
