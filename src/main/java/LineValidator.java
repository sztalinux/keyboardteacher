public class LineValidator {

    public boolean areLinesEqual(String userLine, String fileLine) {

        int numberOfCharsToCompare = 0;

        numberOfCharsToCompare = userLine.length() > fileLine.length() ? userLine.length() : fileLine.length();

        try {
            for (int index = 0; index < numberOfCharsToCompare; index++) {

                if (fileLine.charAt(index) != userLine.charAt(index)) {
                    displayMistakeInfo(userLine, fileLine, index);
                    return false;
                }
            }
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println(DialogTexts.LineLengthErrorText.getMessage());
            return false;
        }
        return true;
    }

    private void displayMistakeInfo(String userLine, String fileLine, int index) {
        System.out.println(DialogTexts.MistakeInUserLineText.getMessage() + (index + 1) + " (wliczając spacje). Wpisano '" + userLine.charAt(index) + "' zamiast '" + fileLine.charAt(index) + "'");
    }
}
