import java.util.Scanner;

public class UserMessenger {

    private final Scanner inputScanner;

    public UserMessenger() {
        inputScanner = new Scanner(System.in);
    }

    public void displayWelcomeMessage() {
        System.out.println(DialogTexts.WelcomeText.getMessage());
    }

    public String getFilePathFromUser() {
        System.out.println("\n" + DialogTexts.AskUserAboutFilePathText.getMessage());
        return inputScanner.nextLine();
    }

    public String getNameFromUser() {
        System.out.println(DialogTexts.AskUserAboutNameText.getMessage());
        return inputScanner.nextLine();
    }

    public String getNextLineFromUser() {
        return inputScanner.nextLine();
    }

    public void displayCongratulationAndTime(String userName, double time){
        System.out.println("\n" + userName + ", " + DialogTexts.CongratulationText.getMessage() + "\n" + DialogTexts.TypingTimeInfoText.getMessage() + time);
    }
}
