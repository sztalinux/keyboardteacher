public class TimeCounter {
    private double timeStart;
    private double timeEnd;

    public void start(){
        timeStart = System.currentTimeMillis();
    }

    public void end(){
        timeEnd = System.currentTimeMillis();
    }

    public double getTimeInSeconds(){
        return (timeEnd - timeStart) / 1000;
    }
}
