import java.sql.Time;
import java.util.ArrayList;
import java.util.Scanner;

public class TypingController {

    private final LineValidator lineValidator;

    private Scanner fileReader;
    private String userName;

    private ArrayList<String> fileLinesArray = new ArrayList<String>();
    private UserMessenger userMessenger;

    public TypingController(Scanner fileReader, String userName, UserMessenger userMessenger) {
        this.fileReader = fileReader;
        this.userName = userName;
        this.lineValidator = new LineValidator();
        this.userMessenger = userMessenger;
    }

    public void startTyping() {
        int index = 0;
        TimeCounter timeCounter = new TimeCounter();
        insertLinesFromFileToArray();
        String userLine = "";

        timeCounter.start();

        while (index < fileLinesArray.size()) {
            System.out.println("\n" + fileLinesArray.get(index));
            userLine = userMessenger.getNextLineFromUser();
            if (lineValidator.areLinesEqual(userLine, fileLinesArray.get(index)))
                index++;
        }

        timeCounter.end();

        userMessenger.displayCongratulationAndTime(userName, timeCounter.getTimeInSeconds());
    }

    private void insertLinesFromFileToArray() { //inserts line by line to ArrayList, without empty lines

        String line = "";
        while (fileReader.hasNextLine()) {
            line = fileReader.nextLine();
            line = line.trim(); //deleting unnecessery white signs from begin and end of line
            if (!line.isEmpty()) {
                fileLinesArray.add(line);
            }
        }
    }
}

