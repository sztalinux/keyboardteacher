import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class FileInitializer {

    private Scanner fileReader;

    public void openFile(UserMessenger userMessenger) {
        boolean isFilePathCorrect;
        do {
            String filePath = userMessenger.getFilePathFromUser();
            isFilePathCorrect = initFileReader(filePath);
        } while (!isFilePathCorrect);
    }

    private boolean initFileReader(String filePath) {
        try {
            fileReader = new Scanner(new FileReader(filePath));
            return true;
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }

    public void closeFileReader() {
        if (fileReader != null) {
            fileReader.close();
        }
    }

    public Scanner getFileReader() {
        return fileReader;
    }
}
