public class ApplicationController {

    public void startApplication() {
        UserMessenger userMessenger = new UserMessenger();
        FileInitializer fileInitializer = new FileInitializer();

        userMessenger.displayWelcomeMessage();
        fileInitializer.openFile(userMessenger);
        String userName = userMessenger.getNameFromUser();

        TypingController typingController = new TypingController(fileInitializer.getFileReader(), userName, userMessenger);
        typingController.startTyping();

        fileInitializer.closeFileReader();
    }
}
