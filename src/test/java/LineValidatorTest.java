import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class LineValidatorTest {

    private static final String CORRECT_TEXT = "correctText";
    private static final String INCORRECT_TEXT = "incorrectText";

    private LineValidator validator;

    @Before
    public void setUp() {
        validator = new LineValidator();
    }

    @Test
    public void testAreLinesEqualForIncorrectUserInput() {
        boolean actualResult = validator.areLinesEqual(INCORRECT_TEXT, CORRECT_TEXT);

        assertThat(actualResult).isEqualTo(false);
    }

    @Test
    public void testAreLinesEqualForCorrectUserInput() {
        boolean actualResult = validator.areLinesEqual(CORRECT_TEXT, CORRECT_TEXT);

        assertThat(actualResult).isEqualTo(true);
    }

}
